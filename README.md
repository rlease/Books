## Books of 2025

### Science Fiction/Fantasy

### Fiction
*Baudolino*, Umberto Eco  

### Nonfiction
*The Moth and the Mountain*, Ed Caesar  

## Books of 2024

### Science Fiction/Fantasy
*Dust*, Hugh Howey  
*The Ministry for the Future*, Kim Stanley Robinson  

### Fiction
*The Hunt for Red October*, Tom Clancy  
*The Covenant of Water*, Abraham Verghese  

### Nonfiction
*In the Garden of the Beasts*, Erik Larson  
*Entangled Life*, Merlin Sheldrake  
*The Nineties*, Chuck Klosterman  
*Mindhunter*, John E Douglas, Mark Olshaker  
*Outlive*, Peter Attia, Bill Gifford  
*Braiding Sweetgrass*, Robin Wall Kimmerer  
*Pathogenisis: A history of the world in 8 plagues*, Jonathan Kennedy  
*The Power of Habit*, Charles Duhigg  
*A Fever in the Heartland*, Timothy Egan  
*Supercommunicators*, Charles Duhigg  
*The Man Who Solved the Market*, Gregory Zuckerman  
*Staff Engineer*, Will Larson  
*An Immense World*, Ed Yong  
*Technofeudalism*, Yanis Varoufakis  
*Apocalypse Never*, Michael Shellenberger  
*The Big Burn*, Timothy Egan    
*Isaac's Storm*, Erik Larson  
*Material World*, Ed Conway  
*Age of Revolutions*, Fareed Zakaria  
*Livewired*, David Eagleman  

## Books of 2023

### Science Fiction/Fantasy
*Sword of Destiny*, Andrzej Sapkowski  
*The Last Wish*, Andrzej Sapkowski  
*The Lost Metal*, Brandon Sanderson  
*Blood of Elves*, Andrzej Sapkowski  
*The Time of Contempt*, Andrzej Sapkowski  
*Project Hail Mary*, Andy Weir  
*Baptism of Fire*, Andrzej Sapkowski  
*The Tower of Swallows*, Andrzej Sapkowski  
*The Lady of the Lake*, Andrzej Sapkowski  
*Season of Storms*, Andrzej Sapkowski  
*Light Bringer*, Pierce Brown  
*Wool*, Hugh Howey  
*Shift*, Hugh Howey  

### Fiction
*All the Light We Cannot See*, Anthony Doerr  
*Cutting for Stone*, Abraham Verghese  

### Nonfiction
*Dead Wake*, Erik Larson  
*Rogues*, Patrick Radden Keefe  
*Taste*, Stanley Tucci  
*Quit*, Annie Duke  
*Start with Why*, Simon Sinek  
*I Contain Multitudes*, Ed Yong  
*Fuzz*, Mary Roach  
*Born to Be Hanged*, Keith Thomson  
*The Lost City of the Monkey God*, Douglas Preston  
*When*, Daniel Pink  
*Shackleton*, Ranulph Fiennes  
*The Guns of August*, Barbara W. Tuchman  
*Making it So*, Patrick Stewart  
*The Wager*, David Grann  
*The Spy and the Traitor*, Ben Macintyre  

## Books of 2022

### Science Fiction/Fantasy
*Leviathan Falls*, James S.A. Corey  
*Iron Age*, Pierce Brown  
*Dark Age*, Pierce Brown  
*Best Served Cold*, Joe Abercrombie  
*The Heroes*, Joe Abercrombie  
*Red Country*, Joe Abercrombie  
*Sharp Ends*, Joe Abercrombie  

### Fiction
*Mythos*, Stephen Fry  
*Beartown*, Frederik Backman  
*Pride and Prejudice*, Jane Austen  
*Dracula*, Bram Stoker  
*Dark Places*, Gillian Flynn  
*The Bell Jar*, Sylvia Plath  
*Turtles All the Way Down*, John Green  

### Nonfiction
*The Dawn of Everything*, David Graeber & David Wengrow  
*A People's History of the United States*, Howard Zinn  
*The Bomber Mafia*, Malcolm Gladwell  
*Night*, Elie Wiesel  
*The Storyteller*, Dave Grohl  
*How to Be Perfect*, Michael Schur  
*The Code Breaker*, Walter Isaacson  
*Underland*, Robert Macfarlane  
*Empire of Pain*, Patrick Radden Keefe  
*Red Famine*, Anne Applebaum  
*Say Nothing*, Patrick Radden Keefe  
*Think Again*, Adam Grant  
*Cultish*, Amanda Montell  
*Mobituaries*, Mo Rocca  
*The Immortal Life of Henrietta Lacks*, Rebecca Skloot  
*The Coddling of the American Mind*, Greg Lukianoff, Jonathan Haidt  
*Moneyball*, Michael Lewis  
*The Anthropocene Reviewed*, John Green  

## Books of 2021

### Science Fiction/Fantasy
*The Golden Compass*, Philip Pullman  
*The Subtle Knife*, Philip Pullman  
*The Amber Spyglass*, Philip Pullman  
*The Stand*, Stephen King  
*Mistborn*, Brandon Sanderson  
*The Well of Ascension*, Brandon Sanderson  
*The Hero of Ages*, Brandon Sanderson  
*The Blade Itself*, Joe Abercrombie  
*The Alloy of Law*, Brandon Sanderson  
*Before They Are Hanged*, Joe Abercrombie  
*Last Argument of Kings*, Joe Abercrombie  
*Shadows of Self*, Brandon Sanderson  
*The Magicians*, Lev Grossman  
*The Bands of Mourning*, Brandon Sanderson  
*The Eye of the World*, Robert Jordan  
*The Great Hunt*, Robert Jordan  
*The Dragon Reborn*, Robert Jordan  
*The Shadow Rising*, Robert Jordan  

### Fiction
*A Man Called Ove*, Fredrik Backman  
*Little Fires Everywhere*, Celeste Ng  
*Sharp Objects*, Gillian Flynn  
*Murder on the Orient Express*, Agatha Christie  
*Death on the Nile*, Agatha Christie  
*The Mysterious Affair at Styles*, Agatha Christie  
*American Gods*, Neil Gaiman  

### Nonfiction
*Just Mercy*, Bryan Stevenson  
*Eat a Peach*, David Chang  
*21 Lessons for the 21st Century*, Yuval Noah Harari  
*The Subtle Art of Not Giving a Fuck*, Mark Manson  
*Atomic Habits*, James Clear  
*Shoe Dog*, Phil Knight  
*Breath*, James Nestor  
*Cosmic Queries*, Neil DeGrasse Tyson  
*Is This Anything?*, Jerry Seinfeld  
*How to Avoid a Climate Disaster*, Bill Gates  
*Guns, Germs, and Steel*, Jared Diamond  
*The Splendid and the Vile*, Erik Larson  
*In a Sunburned Country*, Bill Bryson  
*Empire of the Summer Moon*, S.C. Gwynne  
*The New One Minute Manager*, Ken Blanchard, Spencer Johnson  

## Books of 2020  

### Science Fiction/Fantasy
*The Fifth Season*, N.K. Jemison  
*The Obelisk Gate*, N.K. Jemison  
*The Stone Sky*, N.K. Jemison  
*The Farseer*, Robin Hobb  
*Good Omens*, Terry Pratchett and Neil Gaiman  
*Dark Matter*, Blake Crouch  

### Fiction
*Their Eyes Were Watching God*, Zora Neale Hurston  
*Gone Girl*, Gillian Flynn  
*The Goldfinch*, Donna Tartt  
*Where the Crawdads Sing*, Delia Owens  
*It Can't Happen Here*, Sinclair Lewis  
*Heart of Darkness*, Joseph Conrad  

### Nonfiction
*Enlightenment Now*, Stephen Pinker  
*Poisoner in Chief*, Stephen Kinzer  
*I'll Be Gone in the Dark*, Michelle McNamara  
*Man's Search for Meaning*, Viktor Frankl  
*When Breath Becomes Air*, Paul Kalanithi  
*Dad is Fat*, Jim Gaffigan  
*The Body*, Bill Bryson  
*A Walk in the Woods*, Bill Bryson  
*Talking to Strangers*, Malcolm Gladwell  
*10% Happier*, Dan Harris  
*Grit*, Angela Duckworth  
*Range*, David Epstein  
*Drive*, Daniel Pink  
*Astrophysics for People in a Hurry*, Neil DeGrasse Tyson  
*Stiff*, Mary Roach  
*At Home*, Bill Bryson  
*The Answer Is...*, Alex Trebek  

## Books of 2019  

### Science Fiction/Fantasy  
*Guards! Guards!*, Terry Pratchett  
*The Name of the Wind*, Patrick Rothfuss  
*The Wise Man's Fear*, Patrick Rothfuss  
*The Three Body Problem*, Cixin Liu  
*Leviathan Wakes*, James S.A. Corey  
*Caliban's War*, James S.A. Corey  
*Abaddon's Gate*, James S.A. Corey  
*Cibolla Burn*, James S.A. Corey  
*Nemesis Games*, James S.A. Corey  
*Babylon's Ashes*, James S.A. Corey  
*Perseopolis Rising*, James S.A. Corey  
*Tiamat's Wrath*, James S.A. Corey  
*Exhalation* (short story anthology), Ted Chiang  
*Thrawn*, Timothy Zahn  
*Red Rising*, Pierce Brown  
*The Underground Railroad*, Colson Whitehead  
*Golden Son*, Pierce Brown  
*Morning Star*, Pierce Brown  
*Fire and Blood*, George R.R. Martin  
*The Way of Kings*, Brandon Sanderson  
*Dune Messiah*, Frank Herbert  
*Words of Radiance*, Brandon Sanderson  

### Fiction  
*After the Quake*, (short stories) Haruki Marukami  
*Of Mice and Men*, John Steinbeck  

### Nonfiction  
*Bad Blood*, Jon Carreyrou  
*Hillbilly Elegy*, J.D. Vance  
*The Tipping Point*, Malcolm Gladwell  
*Born to Run*, Christopher McDougall  
*Killers of the Flower Moon*, David Grann  
*Brain on Fire*, Susannah Cahalan  
*Outliers*, Malcolm Gladwell  
*A Short History of Nearly Everything*, Bill Bryson  
*How to Change Your Mind*, Michael Pollan  
*David and Goliath*, Malcolm Gladwell  
*On Writing*, Stephen King  
*Why We Sleep*, Matthew Walker  
*The Omnivore's Dilemma*, Michael Pollan  
*Leonardo da Vinci*, Walter Isaacson  
*Educated*, Tara Westover  
*The Devil in the White City*, Erik Larson  
*Never Split the Difference*, Chris Voss  

## Books of 2018  

*Dune*, Frank Herbert  
*The Jungle*, Upton Sinclair  
*Hyperion*, Dan Simmons  

## Books of 2017

*Title*, Author, Date Finished, Pages  

### Science Fiction/Fantasy  
*Stories of Your Life* (short stories), Ted Chiang, 12/27/2016, 274  
*Ready Player One*, Ernest Cline, 1/24/2017, 372  
*The Man in the High Castle*, Philip K. Dick, 04/14/2017, 229  
*Slaughterhouse Five*, Kurt Vonnegut, 04/16/2017, 275  
*Do Androids Dream of Electric Sheep?*, Philip K. Dick, 04/18/2017, 173  
*Ubik*, Philip K. Dick, 04/21/2017, 190  
*Foundation*, Isaac Asimov, 06/19/2017, 244  
*Foundation and Empire*, Isaac Asimov, 06/20/2017, 244  
*Second Foundation*, Isaac Asimov, 06/23/2017, 241  
*The Moon is a Harsh Mistress*, Robert A. Heinlein, 08/06/2017, 382  
*A Scanner Darkly*, Philip K. Dick, 11/24/2017, 275  
*Harry Potter and the Sorceror's Stone*, J.K. Rowling, 11/25/2017, 309  
*Harry Potter and the Chamber of Secrets*, J.K. Rowling, 11/26/2017, 341  
*Fahrenheit 451*, Ray Bradbury, 12/3/2017, 165  
*The Martian Chronicles*, Ray Bradbury, 12/17/2017, 241  

### Fiction  
*The Alchemist*, Paul Coelho, 12/31/2016, 171  
*One Hundred Years of Solitude*, Gabriel García Márquez, 01/15/2017, 448  
*The Picture of Dorian Gray*, Oscar Wilde, 01/21/2017, 213  
*The Moon is Down*, John Steinbeck, 02/06/2017, 99  
*Cannery Row*, John Steinbeck, 02/11/2017, 129  
*East of Eden*, John Steinbeck, 3/3/2017, 638  
*The Sound and the Fury*, William Faulkner, 03/24/2017, 323  
*The Master and Margarita*, Mikhail Bulgakov, 06/11/2017, 396  
*The Handmaid's Tale*, Margaret Atwood, 07/23/2017, 311  
*The Sun Also Rises*, Ernest Hemingway, 08/20/2017, 251  
*The Old Man and the Sea*, Ernest Hemingway, 08/22/2017, 127  
*Underground Airlines*, Ben Winters, 09/29/2017, 322  
*All the Pretty Horses*, Cormac McCarthy, 10/15/2017, 302  
*Things Fall Apart*, Chinua Achebe, 11/01/2017, 209  
*Blood Meridian*, Cormac McCarthy, 11/13/2017, 351  
*A Clockwork Orange*, Anthony Burgess, 12/25/2017, 212  

### Nonfiction
*Mindstorms*, Seymour Papert, 01/06/2017, 205  
*Awakenings*, Oliver Sacks, 02/04/2017, 201  
*Countdown to Zero Day*, Kim Zetter, 02/20/2017, 406  
*What Customers Want*, Anthony W. Ulwick, 03/11/2017, 176  
*Don't Make Me Think*, Steve Krug, 03/25/2017, 181  
*Thinking Fast and Slow*, Daniel Kahneman, 4/10/2017, 418  
*Nudge*, Richard Thaler, 05/07/2017, 271  
*Blink*, Malcom Gladwell, 05/12/2017, 254  
*Deep Work*, Cal Newport, 05/21/2017, 263  
*Creativity, Inc.*, Ed Catmull, 05/26/2017, 319  
*Moonwalking with Einstein*, Joshua Foer, 06/17/2017, 271  
*First, Break All the Rules*, Marcus Buckingham & Curt Coffman, 06/25/2017, 270    
*The Black Swan*, Nassim Taleb, 07/18/2017, 379  
*Originals*, Adam Grant, 07/29/2017, 254  
*The Five Dysfunctions of a Team*, Patrick Lencioni, 08/23/2017, 224  
*Getting Naked*, Patrick Lencioni, 08/26/2017, 217  
*Sapiens: A Brief History of Humankind*, Yuval Noah Harari, 09/10/2017, 466  
*Predictably Irrational*, Dan Ariely, 09/17/2017, 322  
*The Idea Factory*, Jon Gertner, 10/3/2017, 360  
*High Output Management*, Andrew Grove, 12/20/2017, 232  
*The Alliance*, Reid Hoffman, Ben Casnocha, Chris Yeh, 12/21/2017, 155  

### 2017 Total
52  
